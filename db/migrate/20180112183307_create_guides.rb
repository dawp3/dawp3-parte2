class CreateGuides < ActiveRecord::Migration[5.1]
  def change
    create_table :guides do |t|
      t.string  :title
      t.text    :body
      t.string  :photo
      t.string  :category
      t.integer :trophies
      t.datetime :published_at
      t.integer :user_id

      t.timestamps
    end
  end
end
