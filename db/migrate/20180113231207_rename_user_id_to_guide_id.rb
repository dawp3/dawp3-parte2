class RenameUserIdToGuideId < ActiveRecord::Migration[5.1]
  def change
    rename_column :comments, :user_id, :guide_id
  end
end
