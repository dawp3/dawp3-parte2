class RemovePublishedAt < ActiveRecord::Migration[5.1]
  def change
    remove_column :comments, :published_at
    add_column :comments, :name, :string
  end
end
