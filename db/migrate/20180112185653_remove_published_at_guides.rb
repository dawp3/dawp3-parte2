class RemovePublishedAtGuides < ActiveRecord::Migration[5.1]
  def change
    remove_column :guides, :published_at
  end
end
