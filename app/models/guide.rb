class Guide < ApplicationRecord
  mount_uploader :photo, ImageUploader

  belongs_to :user, optional: true
  has_many :comments

  scope :guide_list, -> {select('guides.id, guides.user_id, guides.photo, guides.title, guides.trophies, guides.category, guides.updated_at, users.name').joins(:user).order('guides.updated_at DESC').limit(10)}
  validates :body, :title, :trophies, :category, presence: {message: "el campo no puede quedar vacío"}
  validates :photo, presence: {message: "Debe añadir una foto"}

  def owned_by?(owner)
    return false unless owner.is_a? User
    user == owner
  end
end
