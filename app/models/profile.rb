class Profile < ApplicationRecord
  mount_uploader :photo, ImageUploader
  belongs_to :user, optional: true

  scope :minirank, -> {select('users.id, users.name, profiles.points, profiles.photo, profiles.bio').joins(:user).order('points DESC').limit(3)}
end
