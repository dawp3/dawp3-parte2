class WelcomeController < ApplicationController
 
  def index
   @users = Profile.all
   @ranking = @users
                    .minirank
  end
end
