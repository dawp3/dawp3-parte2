class ProfilesController < ApplicationController
  def new
    @profile = Profile.new
  end

  def show
    @user = User.find_by_id(params[:id])
    @profile = @user.profile
  end

  def edit
   @profile = Profile.find_by_id(params[:id])
   redirect_to root_path, alert: "Operación no permitida." unless @profile
  end
 
  def update
    @profile = Profile.find_by_id(params[:id])
    respond_to do |format|
      if @profile.update(profile_params)
        format.html { redirect_to @profile, notice: 'Perfil actualizado.' }
      else
        format.html { render :edit }
      end
    end
  end 

  def profile_params
      params.require(:profile).permit(:points, :bio, :categorie, :photo, :user_id)
  end
end
